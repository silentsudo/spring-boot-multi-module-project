package in.silentsudo.awsstore;

import in.silentsudo.storageservice.FileStorageService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
@Qualifier("awsstore")
public class AwsStore implements FileStorageService {
    @Override
    public String store(File file) {
        return "Storing File in AWS Storage";
    }
}
