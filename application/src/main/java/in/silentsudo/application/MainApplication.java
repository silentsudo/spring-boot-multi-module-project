package in.silentsudo.application;

import in.silentsudo.library.MyService;
import in.silentsudo.storageservice.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;

@SpringBootApplication(scanBasePackages = "in.silentsudo")
@RestController
public class MainApplication {

    private final MyService myService;
    private final FileStorageService fileStorageService;
    private final FileStorageService awsStore;

    @Autowired
    public MainApplication(MyService myService,
                           @Qualifier("diskstorage") FileStorageService diskStorageService,
                           @Qualifier("awsstore") FileStorageService awsStore) {
        this.myService = myService;

        fileStorageService = diskStorageService;
        this.awsStore = awsStore;
    }

    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class, args);
    }


    @GetMapping("/")
    public String message() {
        return myService.getMessage();
    }

    @GetMapping("/store")
    public String store() {
        return fileStorageService.store(new File("/home/ashish/remove_outliers"));
    }

    @GetMapping("/awsstore")
    public String awsStore() {
        return awsStore.store(new File("/home/ashish/remove_outliers"));
    }
}

