package in.silentsudo.storageservice;

import java.io.File;

public interface FileStorageService {
    String store(File file);
}
