package in.silentsudo.diskstorageservice;

import in.silentsudo.storageservice.FileStorageService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
@Qualifier("diskstorage")
public class DiskStorageService implements FileStorageService {
    @Override
    public String store(File file) {
        return "Stored file at disk";
    }
}
