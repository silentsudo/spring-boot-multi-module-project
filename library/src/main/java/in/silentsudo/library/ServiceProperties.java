package in.silentsudo.library;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ConfigurationProperties("env")
@PropertySource("classpath:env.properties")
public class ServiceProperties {
    @Getter
    @Setter
    private String message;
}
